//================================================================================================================================
//
// Copyright (c) 2015-2019 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//================================================================================================================================

import OpenGLES
import EasyARSwift

internal enum FrameShader {
    case RGB
    case YUV
}
internal enum RetrieveStatus {
    case Unset
    case Upload
    case Clear
}

func mul(_ lhs: Matrix44F, _ rhs: Vec4F) -> Vec4F
{
    let lhs_data = [lhs.data.0, lhs.data.1, lhs.data.2, lhs.data.3, lhs.data.4, lhs.data.5, lhs.data.6, lhs.data.7, lhs.data.8, lhs.data.9, lhs.data.10, lhs.data.11, lhs.data.12, lhs.data.13, lhs.data.14, lhs.data.15];
    let rhs_data = [rhs.data.0, rhs.data.1, rhs.data.2, rhs.data.3]
    var v: [Float] = [0, 0, 0, 0];
    for i in 0..<4 {
        for k in 0..<4 {
            v[i] += lhs_data[i * 4 + k] * rhs_data[k]
        }
    }
    return Vec4F(v[0], v[1], v[2], v[3])
}

internal class Renderer {
    private let videobackground_vert: String = ""
    + "attribute vec4 coord;\n"
    + "attribute vec2 texCoord;\n"
    + "varying vec2 texc;\n"
    + "\n"
    + "void main(void)\n"
    + "{\n"
    + "    gl_Position = coord;\n"
    + "    texc = texCoord;\n"
    + "}\n"

    private let videobackground_bgr_frag: String = ""
    + "#ifdef GL_ES\n"
    + "precision mediump float;\n"
    + "#endif\n"
    + "uniform sampler2D texture;\n"
    + "varying vec2 texc;\n"
    + "\n"
    + "void main(void)\n"
    + "{\n"
    + "    gl_FragColor = texture2D(texture, texc).bgra;\n"
    + "}\n"

    private let videobackground_rgb_frag: String = ""
    + "#ifdef GL_ES\n"
    + "precision mediump float;\n"
    + "#endif\n"
    + "uniform sampler2D texture;\n"
    + "varying vec2 texc;\n"
    + "\n"
    + "void main(void)\n"
    + "{\n"
    + "    gl_FragColor = texture2D(texture, texc);\n"
    + "}\n"

    private let videobackground_yuv_i420_yv12_frag: String = ""
    + "#ifdef GL_ES\n"
    + "precision highp float;\n"
    + "#endif\n"
    + "uniform sampler2D texture;\n"
    + "uniform sampler2D u_texture;\n"
    + "uniform sampler2D v_texture;\n"
    + "varying vec2 texc;\n"
    + "\n"
    + "void main(void)\n"
    + "{\n"
    + "    float cb = texture2D(u_texture, texc).r - 0.5;\n"
    + "    float cr = texture2D(v_texture, texc).r - 0.5;\n"
    + "    vec3 ycbcr = vec3(texture2D(texture, texc).r, cb, cr);\n"
    + "    vec3 rgb = mat3(1, 1, 1,\n"
    + "        0, -0.344, 1.772,\n"
    + "        1.402, -0.714, 0) * ycbcr;\n"
    + "    gl_FragColor = vec4(rgb, 1.0);\n"
    + "}\n"

    private let videobackground_yuv_nv12_frag: String = ""
    + "#ifdef GL_ES\n"
    + "precision highp float;\n"
    + "#endif\n"
    + "uniform sampler2D texture;\n"
    + "uniform sampler2D uv_texture;\n"
    + "varying vec2 texc;\n"
    + "\n"
    + "void main(void)\n"
    + "{\n"
    + "    vec2 cbcr = texture2D(uv_texture, texc).ra - vec2(0.5, 0.5);\n"
    + "    vec3 ycbcr = vec3(texture2D(texture, texc).r, cbcr);\n"
    + "    vec3 rgb = mat3(1, 1, 1,\n"
    + "        0, -0.344, 1.772,\n"
    + "        1.402, -0.714, 0) * ycbcr;\n"
    + "    gl_FragColor = vec4(rgb, 1.0);\n"
    + "}\n"

    private let videobackground_yuv_nv21_frag: String = ""
    + "#ifdef GL_ES\n"
    + "precision highp float;\n"
    + "#endif\n"
    + "uniform sampler2D texture;\n"
    + "uniform sampler2D uv_texture;\n"
    + "varying vec2 texc;\n"
    + "\n"
    + "void main(void)\n"
    + "{\n"
    + "    vec2 cbcr = texture2D(uv_texture, texc).ar - vec2(0.5, 0.5);\n"
    + "    vec3 ycbcr = vec3(texture2D(texture, texc).r, cbcr);\n"
    + "    vec3 rgb = mat3(1, 1, 1,\n"
    + "        0, -0.344, 1.772,\n"
    + "        1.402, -0.714, 0) * ycbcr;\n"
    + "    gl_FragColor = vec4(rgb, 1.0);\n"
    + "}\n"

    private var yuv_black: [UInt8] = [
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        127, 127,
        127, 127,
        127, 127,
        127, 127
    ]

    private var background_shader_: FrameShader = FrameShader.RGB

    private var initialized_: Bool = false
    private var background_program_: GLuint = 0;
    private var background_texture_id_: GLuint = 0
    private var background_texture_uv_id_: GLuint = 0
    private var background_texture_u_id_: GLuint = 0
    private var background_texture_v_id_: GLuint = 0
    private var background_coord_location_: Int32 = -1
    private var background_texture_location_: Int32 = -1
    private var background_coord_vbo_: GLuint = 0
    private var background_texture_vbo_: GLuint = 0
    private var background_texture_fbo_: GLuint = 0

    private var current_format_: PixelFormat = PixelFormat.Unknown
    private var current_image_size_: Vec2I = Vec2I(0, 0)

    public init() {
    }

    deinit {
        finalize(current_format_)
    }

    public func upload(_ format: PixelFormat, _ width: Int32, _ height: Int32, _ bufferData: OpaquePointer) -> Void {
        var bak_tex: GLuint = 0
        var bak_program: GLuint = 0
        var bak_active_tex: GLuint = 0
        var bak_tex_1: GLuint = 0
        var bak_tex_2: GLuint = 0
        glGetIntegerv(GLenum(GL_CURRENT_PROGRAM), UnsafeMutableRawPointer(&bak_program).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_ACTIVE_TEXTURE), UnsafeMutableRawPointer(&bak_active_tex).assumingMemoryBound(to: GLint.self))
        glActiveTexture(GLenum(GL_TEXTURE0))
        glGetIntegerv(GLenum(GL_TEXTURE_BINDING_2D), UnsafeMutableRawPointer(&bak_tex).assumingMemoryBound(to: GLint.self))
        glActiveTexture(GLenum(GL_TEXTURE1))
        glGetIntegerv(GLenum(GL_TEXTURE_BINDING_2D), UnsafeMutableRawPointer(&bak_tex_1).assumingMemoryBound(to: GLint.self))
        glActiveTexture(GLenum(GL_TEXTURE2))
        glGetIntegerv(GLenum(GL_TEXTURE_BINDING_2D), UnsafeMutableRawPointer(&bak_tex_2).assumingMemoryBound(to: GLint.self))

        defer {
            glActiveTexture(GLenum(GL_TEXTURE0))
            glBindTexture(GLenum(GL_TEXTURE_2D), bak_tex)
            glActiveTexture(GLenum(GL_TEXTURE1))
            glBindTexture(GLenum(GL_TEXTURE_2D), bak_tex_1)
            glActiveTexture(GLenum(GL_TEXTURE2))
            glBindTexture(GLenum(GL_TEXTURE_2D), bak_tex_2)
            glActiveTexture(bak_active_tex)
            glUseProgram(bak_program)
        }

        if current_format_ != format {
            finalize(current_format_);
            if (!initialize(format)) { return }
            current_format_ = format;
        }
        current_image_size_ = Vec2I(width, height)

        switch (background_shader_)
        {
        case FrameShader.RGB:
            glActiveTexture(GLenum(GL_TEXTURE0))
            glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_id_)
            retrieveFrame(format, width, height, bufferData, 0)
        case FrameShader.YUV:
            glActiveTexture(GLenum(GL_TEXTURE0))
            glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_id_)
            retrieveFrame(format, width, height, bufferData, 0)
            if format == PixelFormat.YUV_NV21 || format == PixelFormat.YUV_NV12 {
                glActiveTexture(GLenum(GL_TEXTURE1))
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_uv_id_)
                retrieveFrame(format, width, height, bufferData, 1)
            } else {
                glActiveTexture(GLenum(GL_TEXTURE1))
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_u_id_)
                retrieveFrame(format, width, height, bufferData, 1)
                glActiveTexture(GLenum(GL_TEXTURE2))
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_v_id_)
                retrieveFrame(format, width, height, bufferData, 2)
            }
        }
    }

    public func render(_ imageProjection: Matrix44F) -> Void {
        var bak_blend: GLuint = 0, bak_depth: GLuint = 0, bak_fbo: GLuint = 0, bak_tex: GLuint = 0, bak_arr_buf: GLuint = 0, bak_ele_arr_buf: GLuint = 0, bak_cull: GLuint = 0, bak_program: GLuint = 0, bak_active_tex: GLuint = 0, bak_tex_1: GLuint = 0, bak_tex_2: GLuint = 0
        var bak_viewport: (GLint, GLint, GLint, GLint) = (0, 0, 0, 0)
        glGetIntegerv(GLenum(GL_BLEND), UnsafeMutableRawPointer(&bak_blend).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_DEPTH_TEST), UnsafeMutableRawPointer(&bak_depth).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_CULL_FACE), UnsafeMutableRawPointer(&bak_cull).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_ARRAY_BUFFER_BINDING), UnsafeMutableRawPointer(&bak_arr_buf).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_ELEMENT_ARRAY_BUFFER_BINDING), UnsafeMutableRawPointer(&bak_ele_arr_buf).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_FRAMEBUFFER_BINDING), UnsafeMutableRawPointer(&bak_fbo).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_VIEWPORT), &bak_viewport.0)
        glGetIntegerv(GLenum(GL_CURRENT_PROGRAM), UnsafeMutableRawPointer(&bak_program).assumingMemoryBound(to: GLint.self))
        glGetIntegerv(GLenum(GL_ACTIVE_TEXTURE), UnsafeMutableRawPointer(&bak_active_tex).assumingMemoryBound(to: GLint.self))
        glActiveTexture(GLenum(GL_TEXTURE0))
        glGetIntegerv(GLenum(GL_TEXTURE_BINDING_2D), UnsafeMutableRawPointer(&bak_tex).assumingMemoryBound(to: GLint.self))
        glActiveTexture(GLenum(GL_TEXTURE1))
        glGetIntegerv(GLenum(GL_TEXTURE_BINDING_2D), UnsafeMutableRawPointer(&bak_tex_1).assumingMemoryBound(to: GLint.self))
        glActiveTexture(GLenum(GL_TEXTURE2))
        glGetIntegerv(GLenum(GL_TEXTURE_BINDING_2D), UnsafeMutableRawPointer(&bak_tex_2).assumingMemoryBound(to: GLint.self))

        var va: [GLuint] = [GLuint(bitPattern: -1), GLuint(bitPattern: -1)]
        var bak_va_binding: [GLuint] = [0, 0]
        var bak_va_enable: [Int32] = [0, 0], bak_va_size: [Int32] = [0, 0], bak_va_stride: [Int32] = [0, 0], bak_va_type: [Int32] = [0, 0], bak_va_norm: [Int32] = [0, 0]
        var bak_va_pointer: [UnsafeMutableRawPointer?] = [nil, nil]

        glDisable(GLenum(GL_DEPTH_TEST))
        glDisable(GLenum(GL_BLEND))
        glDisable(GLenum(GL_CULL_FACE))
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), 0)
        glBindBuffer(GLenum(GL_ELEMENT_ARRAY_BUFFER), 0)

        va[0] = GLuint(bitPattern: background_coord_location_)
        va[1] = GLuint(bitPattern: background_texture_location_)
        for i in 0..<2 {
            if va[i] == GLuint(bitPattern: -1) { continue }
            glGetVertexAttribiv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING), UnsafeMutableRawPointer(&bak_va_binding[i]).assumingMemoryBound(to: GLint.self))
            glGetVertexAttribiv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_ENABLED), &bak_va_enable[i])
            glGetVertexAttribiv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_SIZE), &bak_va_size[i])
            glGetVertexAttribiv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_STRIDE), &bak_va_stride[i])
            glGetVertexAttribiv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_TYPE), &bak_va_type[i])
            glGetVertexAttribiv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_NORMALIZED), &bak_va_norm[i])
            glGetVertexAttribPointerv(va[i], GLenum(GL_VERTEX_ATTRIB_ARRAY_POINTER), &bak_va_pointer[i])
        }

        defer {
            if bak_blend != 0 { glEnable(GLenum(GL_BLEND)) }
            if bak_depth != 0 { glEnable(GLenum(GL_DEPTH_TEST)) }
            if bak_cull != 0 { glEnable(GLenum(GL_CULL_FACE)) }

            for i in 0..<2 {
                if bak_va_binding[i] == 0 { continue }
                glBindBuffer(GLenum(GL_ARRAY_BUFFER), bak_va_binding[i])
                if bak_va_enable[i] != 0 {
                    glEnableVertexAttribArray(va[i])
                } else {
                    glDisableVertexAttribArray(va[i])
                    glVertexAttribPointer(va[i], bak_va_size[i], GLenum(bak_va_type[i]), GLboolean(bak_va_norm[i]), bak_va_stride[i], bak_va_pointer[i])
                }
            }

            glBindBuffer(GLenum(GL_ARRAY_BUFFER), bak_arr_buf)
            glBindBuffer(GLenum(GL_ELEMENT_ARRAY_BUFFER), bak_ele_arr_buf)
            glBindFramebuffer(GLenum(GL_FRAMEBUFFER), bak_fbo)
            glActiveTexture(GLenum(GL_TEXTURE0))
            glBindTexture(GLenum(GL_TEXTURE_2D), bak_tex)
            glActiveTexture(GLenum(GL_TEXTURE1))
            glBindTexture(GLenum(GL_TEXTURE_2D), bak_tex_1)
            glActiveTexture(GLenum(GL_TEXTURE2))
            glBindTexture(GLenum(GL_TEXTURE_2D), bak_tex_2)
            glActiveTexture(bak_active_tex)
            glViewport(bak_viewport.0, bak_viewport.1, bak_viewport.2, bak_viewport.3)
            glUseProgram(bak_program)
        }

        glUseProgram(background_program_)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), background_coord_vbo_)

        glEnableVertexAttribArray(GLuint(background_coord_location_))
        glVertexAttribPointer(GLuint(background_coord_location_), 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, nil)

        var vertices: [GLfloat] = [
            -1,  -1,  0,
            1,  -1,  0,
            1,   1,  0,
            -1, 1,  0,
        ]

        let v0 = mul(imageProjection, Vec4F(vertices[0], vertices[1], vertices[2], 1))
        let v1 = mul(imageProjection, Vec4F(vertices[3], vertices[4], vertices[5], 1))
        let v2 = mul(imageProjection, Vec4F(vertices[6], vertices[7], vertices[8], 1))
        let v3 = mul(imageProjection, Vec4F(vertices[9], vertices[10], vertices[11], 1))
        vertices[0] = v0.data.0
        vertices[1] = v0.data.1
        vertices[2] = v0.data.2
        vertices[3] = v1.data.0
        vertices[4] = v1.data.1
        vertices[5] = v1.data.2
        vertices[6] = v2.data.0
        vertices[7] = v2.data.1
        vertices[8] = v2.data.2
        vertices[9] = v3.data.0
        vertices[10] = v3.data.1
        vertices[11] = v3.data.2

        glBufferData(GLenum(GL_ARRAY_BUFFER), 4 * 12, vertices, GLenum(GL_DYNAMIC_DRAW))

        glBindBuffer(GLenum(GL_ARRAY_BUFFER), background_texture_vbo_)
        glEnableVertexAttribArray(GLuint(background_texture_location_))
        glVertexAttribPointer(GLuint(background_texture_location_), 2, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, nil)

        switch (background_shader_)
        {
        case FrameShader.RGB:
            glActiveTexture(GLenum(GL_TEXTURE0))
            glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_id_)
        case FrameShader.YUV:
            glActiveTexture(GLenum(GL_TEXTURE0))
            glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_id_)
            if current_format_ == PixelFormat.YUV_NV21 || current_format_ == PixelFormat.YUV_NV12 {
                glActiveTexture(GLenum(GL_TEXTURE1))
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_uv_id_)
            } else {
                glActiveTexture(GLenum(GL_TEXTURE1))
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_u_id_)
                glActiveTexture(GLenum(GL_TEXTURE2))
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_v_id_)
            }
        }

        glDrawArrays(GLenum(GL_TRIANGLE_FAN), 0, 4)
    }

    private func retrieveFrame(_ format: PixelFormat, _ width: Int32, _ height: Int32, _ bufferData: OpaquePointer, _ retrieve_count: Int) -> Void {
        if retrieve_count == 0 {
            if (width & 0x3) != 0 {
                if (width & 0x1) != 0 {
                    glPixelStorei(GLenum(GL_UNPACK_ALIGNMENT), 1)
                } else {
                    glPixelStorei(GLenum(GL_UNPACK_ALIGNMENT), 2)
                }
            }

            switch (background_shader_)
            {
            case FrameShader.RGB:
                switch (format)
                {
                case PixelFormat.Unknown:
                    glBindTexture(GLenum(GL_TEXTURE_2D), 0)
                case PixelFormat.Gray:
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_LUMINANCE, width, height, 0, GLenum(GL_LUMINANCE), GLenum(GL_UNSIGNED_BYTE), UnsafeRawPointer(bufferData))
                case PixelFormat.BGR888:
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_RGB, width, height, 0, GLenum(GL_RGB), GLenum(GL_UNSIGNED_BYTE), UnsafeRawPointer(bufferData))
                case PixelFormat.RGB888:
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_RGB, width, height, 0, GLenum(GL_RGB), GLenum(GL_UNSIGNED_BYTE), UnsafeRawPointer(bufferData))
                case PixelFormat.RGBA8888:
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_RGBA, width, height, 0, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), UnsafeRawPointer(bufferData))
                case PixelFormat.BGRA8888:
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_RGBA, width, height, 0, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), UnsafeRawPointer(bufferData))
                default:
                    glBindTexture(GLenum(GL_TEXTURE_2D), 0)
                }
            case FrameShader.YUV:
                if format == PixelFormat.YUV_NV21 || format == PixelFormat.YUV_NV12 ||
                    format == PixelFormat.YUV_I420 || format == PixelFormat.YUV_YV12 {
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_LUMINANCE, width, height, 0, GLenum(GL_LUMINANCE), GLenum(GL_UNSIGNED_BYTE), UnsafeRawPointer(bufferData))
                } else {
                    glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_LUMINANCE, 4, 4, 0, GLenum(GL_LUMINANCE), GLenum(GL_UNSIGNED_BYTE), &yuv_black[0])
                }
            }
        } else if (retrieve_count == 1 || retrieve_count == 2) {
            if (background_shader_ != FrameShader.YUV) { return }
            if (width & 0x7) != 0 {
                if (width & 0x3) != 0 {
                    glPixelStorei(GLenum(GL_UNPACK_ALIGNMENT), 1)
                } else {
                    glPixelStorei(GLenum(GL_UNPACK_ALIGNMENT), 2)
                }
            }
            var data: UnsafeMutableRawPointer? = nil
            var type: GLenum = GLenum(GL_LUMINANCE_ALPHA)
            if format == PixelFormat.YUV_NV21 || format == PixelFormat.YUV_NV12 {
                data = UnsafeMutableRawPointer(bufferData).advanced(by: Int(width * height))
            } else if format == PixelFormat.YUV_I420 {
                type = GLenum(GL_LUMINANCE)
                if retrieve_count == 1 { //U
                    data = UnsafeMutableRawPointer(bufferData).advanced(by: Int(width * height))
                } else if (retrieve_count == 2) { //V
                    data = UnsafeMutableRawPointer(bufferData).advanced(by: Int(width * height * 5 / 4))
                }
            } else if format == PixelFormat.YUV_YV12 {
                type = GLenum(GL_LUMINANCE)
                if retrieve_count == 1 { //U
                    data = UnsafeMutableRawPointer(bufferData).advanced(by: Int(width * height * 5 / 4))
                } else if (retrieve_count == 2) { //V
                    data = UnsafeMutableRawPointer(bufferData).advanced(by: Int(width * height))
                }
            }
            glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GLint(type), width / 2, height / 2, 0, type, GLenum(GL_UNSIGNED_BYTE), data)
        }
    }

    private func initialize(_ format: PixelFormat) -> Bool {
        if format == PixelFormat.Unknown { return false }
        background_program_ = glCreateProgram()
        let vertShader = glCreateShader(GLenum(GL_VERTEX_SHADER))
        videobackground_vert.utf8CString.withUnsafeBufferPointer { p in
            var s = p.baseAddress
            glShaderSource(vertShader, 1, &s, nil)
        }
        glCompileShader(vertShader)
        let fragShader = glCreateShader(GLenum(GL_FRAGMENT_SHADER))

        switch (format) {
        case PixelFormat.Gray, PixelFormat.RGB888, PixelFormat.RGBA8888:
            background_shader_ = FrameShader.RGB
            videobackground_rgb_frag.utf8CString.withUnsafeBufferPointer { p in
                var s = p.baseAddress
                glShaderSource(fragShader, 1, &s, nil)
            }
        case PixelFormat.BGR888, PixelFormat.BGRA8888:
            background_shader_ = FrameShader.RGB
            videobackground_bgr_frag.utf8CString.withUnsafeBufferPointer { p in
                var s = p.baseAddress
                glShaderSource(fragShader, 1, &s, nil)
            }
        case PixelFormat.YUV_NV21:
            background_shader_ = FrameShader.YUV
            videobackground_yuv_nv21_frag.utf8CString.withUnsafeBufferPointer { p in
                var s = p.baseAddress
                glShaderSource(fragShader, 1, &s, nil)
            }
        case PixelFormat.YUV_NV12:
            background_shader_ = FrameShader.YUV
            videobackground_yuv_nv12_frag.utf8CString.withUnsafeBufferPointer { p in
                var s = p.baseAddress
                glShaderSource(fragShader, 1, &s, nil)
            }
        case PixelFormat.YUV_I420, PixelFormat.YUV_YV12:
            background_shader_ = FrameShader.YUV
            videobackground_yuv_i420_yv12_frag.utf8CString.withUnsafeBufferPointer { p in
                var s = p.baseAddress
                glShaderSource(fragShader, 1, &s, nil)
            }
        default:
            break
        }

        glCompileShader(fragShader)
        glAttachShader(background_program_, vertShader)
        glAttachShader(background_program_, fragShader)

        glLinkProgram(background_program_)
        glDeleteShader(vertShader)
        glDeleteShader(fragShader)
        var linkstatus: GLint = 0

        glGetProgramiv(background_program_, GLenum(GL_LINK_STATUS), &linkstatus)
        glUseProgram(background_program_)
        background_coord_location_ = glGetAttribLocation(background_program_, "coord")
        background_texture_location_ = glGetAttribLocation(background_program_, "texCoord")

        glGenBuffers(1, &background_coord_vbo_)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), background_coord_vbo_)
        let coord: [GLfloat] = [-1, -1, 0, 1, -1, 0, 1, 1, 0, -1, 1, 0]
        coord.withUnsafeBytes { p in
            glBufferData(GLenum(GL_ARRAY_BUFFER), p.count, p.baseAddress, GLenum(GL_DYNAMIC_DRAW))
        }
        glGenBuffers(1, &background_texture_vbo_)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), background_texture_vbo_)
        let texcoord: [GLfloat] = [0, 1, 1, 1, 1, 0, 0, 0] //input texture data is Y-inverted
        texcoord.withUnsafeBytes { p in
            glBufferData(GLenum(GL_ARRAY_BUFFER), p.count, p.baseAddress, GLenum(GL_STATIC_DRAW))
        }

        glUniform1i(glGetUniformLocation(background_program_, "texture"), 0)
        glGenTextures(1, &background_texture_id_)
        glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_id_)
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR)
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MAG_FILTER), GL_LINEAR)
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE)
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE)

        switch (background_shader_) {
        case FrameShader.RGB:
            break
        case FrameShader.YUV:
            if format == PixelFormat.YUV_NV21 || format == PixelFormat.YUV_NV12 {
                glUniform1i(glGetUniformLocation(background_program_, "uv_texture"), 1)
                glGenTextures(1, &background_texture_uv_id_)
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_uv_id_)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MAG_FILTER), GL_LINEAR)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE)
            } else {
                glUniform1i(glGetUniformLocation(background_program_, "u_texture"), 1)
                glGenTextures(1, &background_texture_u_id_)
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_u_id_)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MAG_FILTER), GL_LINEAR)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE)

                glUniform1i(glGetUniformLocation(background_program_, "v_texture"), 2)
                glGenTextures(1, &background_texture_v_id_)
                glBindTexture(GLenum(GL_TEXTURE_2D), background_texture_v_id_)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MAG_FILTER), GL_LINEAR)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE)
                glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE)
            }
        }
        glGenFramebuffers(1, &background_texture_fbo_)
        initialized_ = true
        return true
    }

    private func finalize(_ format: PixelFormat) -> Void {
        if (!initialized_) { return }

        glDeleteProgram(background_program_)
        glDeleteBuffers(1, &background_coord_vbo_)
        glDeleteBuffers(1, &background_texture_vbo_)
        glDeleteFramebuffers(1, &background_texture_fbo_)
        glDeleteTextures(1, &background_texture_id_)
        switch (background_shader_) {
        case FrameShader.RGB:
            break;
        case FrameShader.YUV:
            if (format == PixelFormat.YUV_NV21 || format == PixelFormat.YUV_NV12)
            {
                glDeleteTextures(1, &background_texture_uv_id_);
            }
            else
            {
                glDeleteTextures(1, &background_texture_u_id_);
                glDeleteTextures(1, &background_texture_v_id_);
            }
        }
        initialized_ = false
    }
}
