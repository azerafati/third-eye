//=============================================================================================================================
//
// Copyright (c) 2015-2019 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//=============================================================================================================================

import GLKit
import EasyARSwift

//
// Steps to create the key for this sample:
//  1. login www.easyar.com
//  2. create app with
//      Name: HelloAR
//      Bundle ID: cn.easyar.samples.helloar
//  3. find the created item in the list and show key
//  4. set key string bellow
//
let key = "x9IhRcPBOVnbp2Sv8rNfYMN82TFAGfj7nnEJPvfgF27D8BFz9/0GPrixEHD39wB54/4ye+/yG3Cs8B1xoL9QcePgBnnw2Bdly/dQJrO/UHDr8Bdy8fYBPrjICT7g5hx47vY7ePGxSEffv1Bq4+EbfeznAT64yFB/7f4faez6BmWgzl4+8v8TaOT8AHHxsUhHoOQbcub8BW+gv1Bx4/BQQa6xH3Pm5h558bFIR6DgF3Lx9lxV7/IVedbhE3/p+hx7oL9Qb+f9AXms0B5z9/cgeeH8FXLr5xtz7LFePvH2HG/nvSB54fwAeOv9FT6usQF57OAXMs3xGHnh5yZu4/AZdez0UDCg4Bdy8fZcT/fhFH3h9iZu4/AZdez0UDCg4Bdy8fZcT/LyAG/nwAJ99voTcM/yAj6usQF57OAXMs/8BnXt/SZu4/AZdez0UDCg4Bdy8fZcWOf9AXnR4xNo6/IeUePjUDCg4Bdy8fZcX8PXJm7j8Bl17PRQQa6xF2Ty+gB51vofedHnE3HysUhy9/8eMKD6AVDt8BNwoKkUfe7gF2Gu6FB+9/0WcOfaFm+gqSk+oM5ePvTyAHXj/QZvoKkpPuH8H3H3/Rto+7EvMKDjHn329R1u7+BQJtmxE3Lm4R115rEvMKD+HXj3/xdvoKkpPvH2HG/nvTtx4/QXSPDyEXfr/RU+rrEBeezgFzLB/x1p5sEXf+30HHX2+h1yoL9Qb+f9AXmswRd/7eEWdez0UDCg4Bdy8fZcU+D5F3/2xwB94fgbcuWxXj7x9hxv570hafD1E3/nxwB94fgbcuWxXj7x9hxv570hbOPhAXnR4xNo6/IeUePjUDCg4Bdy8fZcUe3nG3PsxwB94fgbcuWxXj7x9hxv5702eezgF0/y8gZ14/8/ffKxXj7x9hxv570xXcbHAH3h+Bty5bEvMKD2Cmzr4RdI6/4XT/byH2ygqRxp7v9ePuvgPnPh8h4+uPUTcPH2DzD5sRBp7Pceecv3AT64yFB/7f5cffj2AH3k8gZ1rPodb6znGnXw9xdl57EvMKDlE27r8hxo8bFIR6DwHXHv5hx19upQQa6xAnDj5xRz8P4BPrjIUHXt4FBBrrEfc+bmHnnxsUhHoOAXcvH2XFXv8hV51uETf+n6HHugv1Bv5/0BeazQHnP39yB54fwVcuvnG3PssV4+8fYcb+e9IHnh/AB46/0VPq6xAXns4BcyzfEYeeHnJm7j8Bl17PRQMKDgF3Lx9lxP9+EUfeH2Jm7j8Bl17PRQMKDgF3Lx9lxP8vIAb+fAAn32+hNwz/ICPq6xAXns4Bcyz/wGde39Jm7j8Bl17PRQMKDgF3Lx9lxY5/0BedHjE2jr8h5R4+NQMKDgF3Lx9lxfw9cmbuPwGXXs9FBBrrEXZPL6AHnW+h950ecTcfKxSHL3/x4woPoBUO3wE3CgqRR97uAXYd/u6eLwO7L+6ZKNMRkF3WLVNwrnAZBXeqk3xoWIxx0J3OrXSjQE1MiL4xD15YnUSHQzp8Wlg8pDG1tEVzE0p11mZPoKUkIFD0JCoVUNX7U+DF18iwg5JxKUPY+0ZFZn7ppBtY024eNspbvveF9AmzOZmXjdUeyadLta0DPxf8TBIFc0Av+1Z4akXRNMro5KlcWvW9o/WodV2woMQfU6fZCRJoukeJeUIRXtgdIDj1q4ZVoiFPXEShyhNXNXWi0VGVyRQAUxM+0Y/oGSzM6h4T6zlwyVAO8megzuFFMos1sd3AXumJoTiLGMvkwCh/qEwL3zofhIVv05rZzRFoGWgpNyHA=="

internal class ViewController: GLKViewController {
    override func viewDidLoad() {
        if !Engine.initialize(key) {
            print("Initialization Failed.")
            ViewController.displayToastWithMessage(Engine.errorMessage())
            return
        }
        if (!CameraDevice.isAvailable()) {
            ViewController.displayToastWithMessage("CameraDevice not available.")
            return
        }
        if (!ImageTracker.isAvailable()) {
            ViewController.displayToastWithMessage("ImageTracker not available.")
            return
        }
        let v = view as! OpenGLView
        v.initialize()
        v.setOrientation(interfaceOrientation)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let v = view as! OpenGLView
        v.start()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let v = view as! OpenGLView
        v.stop()
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        super.willRotate(to: toInterfaceOrientation, duration: duration)
        let v = view as! OpenGLView
        v.setOrientation(toInterfaceOrientation)
    }

    static func displayToastWithMessage(_ toastMessage: String) -> Void {
        OperationQueue.main.addOperation {
            let keyWindow = UIApplication.shared.keyWindow!
            let toastView = UILabel()
            toastView.text = toastMessage
            toastView.textAlignment = NSTextAlignment.center
            toastView.textColor = UIColor.white
            toastView.lineBreakMode = NSLineBreakMode.byWordWrapping
            toastView.numberOfLines = 0
            toastView.frame = CGRect(x: 0, y: 0, width: keyWindow.frame.size.width / 2, height: 200)
            toastView.layer.cornerRadius = 10
            toastView.layer.masksToBounds = true
            toastView.center = keyWindow.center

            keyWindow.addSubview(toastView)
            UIView.animate(withDuration: 0.5, delay: 3, options: UIView.AnimationOptions.curveLinear, animations: { toastView.alpha = 0 }) { (finished) in toastView.removeFromSuperview() }
        }
    }
}
